FROM php:7-fpm

COPY . /app

RUN rm /usr/local/etc/php-fpm.d/www.conf.default

RUN echo 'php_admin_value[error_log] = /proc/self/fd/2' >> /usr/local/etc/php-fpm.d/docker.conf
RUN echo 'php_admin_flag[log_errors] = on' >> /usr/local/etc/php-fpm.d/docker.conf

RUN chown -R www-data:www-data /app
