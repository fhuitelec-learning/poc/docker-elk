FROM nginx:latest

COPY . /app

COPY ./infrastructure/development/configuration/nginx/web-app.conf /etc/nginx/conf.d/web-app.conf
RUN rm /etc/nginx/conf.d/default.conf
