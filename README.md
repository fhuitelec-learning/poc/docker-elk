# Docker Swarm Monitoring

Forked from [this repository](https://github.com/botleg/swarm-logging) by **Hanzel Jesheen** for his article [Log Management for Docker Swarm with ELK Stack](https://botleg.com/stories/log-management-of-docker-swarm-with-elk-stack/).

An issue about my methodology and what I learned is available [here](https://gitlab.com/fhuitelec-learning/learning-wish-list/issues/3).

## Some context

* `Elasticsearch`: Database to store the log data and query for it.
* `Logstash`: Log processor that ingests data from multiple sources and feeds it to Elasticsearch.
* `Logspout`: Logspout is deployed to all hosts to collect the logs from docker daemon and feed it to logstash.
* `Kibana`: Web UI to display Elasticsearch data.

## Getting started

See [this comment](https://gitlab.com/fhuitelec-learning/learning-wish-list/issues/3#note_40530612), if you want to play around comfortably, edit the docker-compose files to use raw `docker-compose` and not a `swarm` cluster.

### `docker-machine`

```bash
# Create the docker-machine on Digital ocean
docker-machine create \
    --driver digitalocean \
    --digitalocean-access-token ${DO_TOKEN} \
    --digitalocean-size 4gb \
    --digitalocean-ssh-key-fingerprint ${SSH_KEY_ID} \
    elk-manager
# Some tweak for Elasticsearch
docker-machine ssh elk-manager sudo sysctl -w vm.max_map_count=262144
```

### `pulls`

_To avoid logspout to fail because it waits too long for Elasticsearch._

```bash
docker pull docker.elastic.co/logstash/logstash:5.6.0 \
    && docker pull docker.elastic.co/elasticsearch/elasticsearch:5.6.0 \
    && docker pull docker.elastic.co/kibana/kibana:5.6.0
```

### Run web-app

```bash
# Change FS context
cd web-app
# Build and push docker images because we're on a swarm cluster
docker build -t fhuitelec/web-app-php:latest -f infrastructure/development/dockerfiles/php.Dockerfile .
docker build -t fhuitelec/web-app-nginx:latest -f infrastructure/development/dockerfiles/nginx.Dockerfile .
docker push fhuitelec/web-app-php:latest
docker push fhuitelec/web-app-nginx:latest
# Deploy the web-app
docker stack deploy -c infrastructure/development/docker-compose.yml
# Test it
open http://(docker-machine ip elk-manager):8080
```

### Run ELK stack

```bash
# Create config resources
docker config logstash_conf ./config/logstash/logstash.conf
docker config create logstash_php-fpm_grok_pattern ./config/logstash/patterns/php_fpm
# Run the ELK stack
docker stack deploy -c docker-stack.yml elk
# Wait ~5 minutes and test it
open http://(docker-machine ip elk-manager)
```
